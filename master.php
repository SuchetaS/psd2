<?php
if(isset($_GET['p'])){
    $pageName = $_GET['p'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $pageName; ?></title>
	
	<!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="./UI-SamplePages/http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="./UI-SamplePages/http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="./UI-SamplePages/http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon--> 
    <link rel="apple-touch-icon" sizes="72x72" href="./UI-SamplePages/http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon--> 
    <link rel="apple-touch-icon" sizes="57x57" href="./UI-SamplePages/http://placehold.it/57.png/000/fff">
	
	<!-- Styles -->
    <link href="./UI-Tools/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/themify-icons.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/weather-icons.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/mmc-chat.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/sidebar.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/simdahs.css" rel="stylesheet">
    <link href="./UI-Tools/css/style.css" rel="stylesheet">


</head>

<body>

    <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
        <?php include('sidebar.html'); ?>
    </div><!-- /# sidebar -->

    <div class="header">
        <?php include('header.html'); ?>
    </div>

	<div class="chat-sidebar">
        <?php include('chat.html'); ?>
    </div>
    <!-- END chat Sidebar-->

    <div class="content-wrap">
        <?php
            if($pageName == "bank-dashboard"){
                include('dash.html');
            } 
            if($pageName == "customer-dashboard"){
                include('debtors-customer.html');
            }      
        ?>
        <?php //include('dash.html'); ?>
    </div><!-- /# content wrap -->
	
    
    <!-- Scripts -->
    <script src="./UI-Tools/js/lib/jquery.min.js"></script><!-- jquery vendor -->
    <script src="./UI-Tools/js/lib/jquery.nanoscroller.min.js"></script><!-- nano scroller -->    
    <script src="./UI-Tools/js/lib/sidebar.js"></script><!-- sidebar -->
    <script src="./UI-Tools/js/lib/bootstrap.min.js"></script><!-- bootstrap -->
    <script src="./UI-Tools/js/lib/mmc-common.js"></script>
    <script src="./UI-Tools/js/lib/mmc-chat.js"></script>
    <!--  Chart js -->
    <!-- <script src="./UI-Tools/js/lib/chart-js/Chart.bundle.js"></script>
    <script src="./UI-Tools/js/lib/chart-js/chartjs-init.js"></script> -->
    
    <!-- // Chart js -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css" >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>



    <script src="./UI-Tools/js/lib/sparklinechart/jquery.sparkline.min.js"></script><!-- scripit init-->
    <script src="./UI-Tools/js/lib/sparklinechart/sparkline.init.js"></script><!-- scripit init-->
    
    <!--  Datamap -->
    <script src="./UI-Tools/js/lib/datamap/d3.min.js"></script>
    <script src="./UI-Tools/js/lib/datamap/topojson.js"></script>
    <script src="./UI-Tools/js/lib/datamap/datamaps.world.min.js"></script>
    <script src="./UI-Tools/js/lib/datamap/datamap-init.js"></script>
    <!-- // Datamap -->
    <script src="./UI-Tools/js/lib/weather/jquery.simpleWeather.min.js"></script>   
    <script src="./UI-Tools/js/lib/weather/weather-init.js"></script>
    <script src="./UI-Tools/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="./UI-Tools/js/lib/owl-carousel/owl.carousel-init.js"></script>
    <!-- <script src="./UI-Tools/js/scripts.js"></script> --><!-- scripit init-->

    <!-- chart js input -->
    <script type="text/javascript">
        new Chart(document.getElementById("bar-chart-horizontal"), {
            type: 'horizontalBar',
            data: {
              labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
              datasets: [
                {
                  label: "Population (millions)",
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                  data: [2478,5267,734,784,433]
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Predicted world population (millions) in 2050'
              }
            }
        });
        new Chart(document.getElementById("line-chart"), {
            type: 'line',
            data: {
                labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
                datasets: [{ 
                data: [86,114,106,106,107,111,133,221,783,2478],
                label: "Africa",
                borderColor: "#3e95cd",
                fill: false
              }, { 
                data: [282,350,411,502,635,809,947,1402,3700,5267],
                label: "Asia",
                borderColor: "#8e5ea2",
                fill: false
              }, { 
                data: [168,170,178,190,203,276,408,547,675,734],
                label: "Europe",
                borderColor: "#3cba9f",
                fill: false
              }, { 
                data: [40,20,10,16,24,38,74,167,508,784],
                label: "Latin America",
                borderColor: "#e8c3b9",
                fill: false
              }, { 
                data: [6,3,2,2,7,26,82,172,312,433],
                label: "North America",
                borderColor: "#c45850",
                fill: false
              }
            ]
          },
          options: {
            title: {
              display: true,
              text: 'World population per region (in millions)'
            }
          }
        });

        var doughnutCanvas = document.getElementById("doughnut-chart");
        var ctxP = doughnutCanvas.getContext('2d');
        var myClickDoughnut = new Chart(ctxP, {
            type: 'doughnut',
            data: {
              labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
              datasets: [
                {
                  //label: "Population (millions)",
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                  data: [2478,5267,734,784,433]
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Predicted world population (millions) in 2050'
              }
            }
        });

        doughnutCanvas.onclick = function(e) {
            alert("custom click");
           var slice = myClickDoughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           alert("label: "+ label);
           switch (label) {
              // add case for each label/slice
              case 'Africa':
                 alert('clicked on Africa');
                 //window.open('www.example.com/foo');
                 break;
              case 'Asia':
                 alert('clicked on Asia');
                 //window.open('www.example.com/bar');
                 break;
              // add rests ...
           }
        }

        var canvasP = document.getElementById("pieChart");
        var ctxP = canvasP.getContext('2d');
        var myPieChart = new Chart(ctxP, {
           type: 'pie',
           data: {
              labels: ["Värde 1", "Värde 2", "Värde 3", "Värde 4", "Värde 5", "Värde 6", "Värde 7"],
              datasets: [{
                 data: [1, 5, 10, 20, 50, 70, 50],
                 backgroundColor: ["#64B5F6", "#FFD54F", "#2196F3", "#FFC107", "#1976D2", "#FFA000", "#0D47A1"],
                 hoverBackgroundColor: ["#B2EBF2", "#FFCCBC", "#4DD0E1", "#FF8A65", "#00BCD4", "#FF5722", "#0097A7"]
              }]
           },
           options: {
              legend: {
                 display: true,
                 position: "right"
              }
           }
        });

        canvasP.onclick = function(e) {
           var slice = myPieChart.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Värde 5':
                 alert('clicked on slice 5');
                 //window.open('www.example.com/foo');
                 break;
              case 'Värde 6':
                 alert('clicked on slice 6');
                 //window.open('www.example.com/bar');
                 break;
              // add rests ...
           }
        }

        

    </script>
<?php include('footer.html'); ?>
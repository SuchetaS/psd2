<?php require('header.php'); ?>

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-0">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Accounts dashboard</h1>
                            </div>
                        </div>
                    </div><!-- /# column -->
                    <div class="col-lg-4 p-0">
                        <div class="page-header">
                            <div class="page-title">
                                <!-- <ol class="breadcrumb text-right">
                                    <li><a href="./UI-SamplePages/#">Accounts dashboard</a></li>
                                    <li class="active">Home</li>
                                </ol> -->
                            </div>
                        </div>
                    </div><!-- /# column -->
                </div><!-- /# row -->
                <div class="main-content">
					<div class="row"> 
						<div class="col-lg-8">
							<div class="card alert">
								<div class="card-header bg-success text-white">
									<h4>Earning Graph</h4>
									<!-- <div class="card-header-right-icon">
										<ul>
											<li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li> 
										</ul>
									</div> -->
								</div>
								<div class="sales-chart">
									<canvas id="bar-chart-horizontal" width="800" height="450"></canvas>
								</div>
							</div>
						</div><!-- /# column -->
                        <div class="col-lg-4">

                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header bg-info text-white">
                                        <h4>Completed</h4>

                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="">
                                                        <h5>Total document</h5>
                                                        <p>The document dummy text here</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <div class="">

                                                        <button type="button" class="btn btn-lg btn-rounded btn-info">
                                                            23
                                                        </button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /# card -->
                            </div><!-- /# column -->


                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header bg-default text-white">
                                        <h4>Approved </h4>

                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="">
                                                        <h5>Total document</h5>
                                                        <p>The document dummy text here</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <div class="">

                                                        <button type="button" class="btn btn-lg btn-rounded btn-default">
                                                            76
                                                        </button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /# card -->
                            </div><!-- /# column -->

                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header bg-warning text-white">
                                        <h4>Under process</h4>

                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="">
                                                        <h5>Total document</h5>
                                                        <p>The document dummy text here</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <div class="">

                                                        <button type="button" class="btn btn-lg btn-rounded btn-warning">
                                                            36
                                                        </button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /# card -->
                            </div><!-- /# column -->
                                
                            
                        </div>
                    </div><!-- /# row -->


					<div class="row">
                        <div class="col-lg-8">
                            <div class="card alert">
                                <div class="card-header bg-dark text-white">
                                    <h4>Expences</h4>
                                    <!-- <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li> 
                                        </ul>
                                    </div> -->
                                </div>
                                <div class="sales-chart">
                                    <canvas id="line-chart" width="800" height="450"></canvas>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4">
                            <div class="card alert">
                                <div class="card-header bg-primary text-white">
                                    <h4>Timeline</h4>
                                    <!-- <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li> 
                                        </ul>
                                    </div> -->
                                </div>
                                <div class="card-body">
                                    <ul class="timeline">
                                        <li>
                                            <div class="timeline-badge primary"><i class="fa fa-smile-o"></i></div>
                                            <div class="timeline-panel">
                                                <div class="timeline-heading">
                                                    <h5 class="timeline-title">You deleted homepage.psd</h5>
                                                </div>
                                                <div class="timeline-body">
                                                    <p>10 minutes ago</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="timeline-badge warning"><i class="fa fa-sun-o"></i></div>
                                            <div class="timeline-panel">
                                                <div class="timeline-heading">
                                                    <h5 class="timeline-title">You change your profile picture</h5>
                                                </div>
                                                <div class="timeline-body">
                                                    <p>20 minutes ago</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="timeline-badge danger"><i class="fa fa-times-circle-o"></i></div>
                                            <div class="timeline-panel">
                                                <div class="timeline-heading">
                                                    <h5 class="timeline-title">You followed john doe</h5>
                                                </div>
                                                <div class="timeline-body">
                                                    <p>30 minutes ago</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="timeline-badge success"><i class="fa fa-check-circle-o"></i></div>
                                            <div class="timeline-panel">
                                                <div class="timeline-heading">
                                                    <h5 class="timeline-title">Some new content has been added. </h5>
                                                </div>
                                                <div class="timeline-body">
                                                    <p>15 minutes ago</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div><!-- /# column -->
						
					</div><!-- /# row -->

                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card alert">
                                <div class="card-header bg-pink text-white">
                                    <h4>Sales</h4>
                                    <!-- <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li> 
                                        </ul>
                                    </div> -->
                                </div>
                                <div class="sales-chart">
                                    <!-- <canvas id="doughnut-chart" width="800" height="450"></canvas> -->
                                    <canvas id="pieChart"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card alert">
                                <div class="card-header bg-success text-white">
                                    <h4>Todo List</h4>
                                    <div class="card-header-right-icon">
                                        <!-- <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li> 
                                        </ul> -->
                                    </div>
                                </div>
                                <div class="todo-list">
                                    <div class="tdl-holder">
                                        <div class="tdl-content">
                                            <ul>
                                                <li><label><input type="checkbox"><i></i><span>Decorate Kitchen Room</span><a href='#' class="ti-close"></a></label></li>
                                                <li><label><input type="checkbox" checked><i></i><span>Delevery Pizza</span><a href='#' class="ti-close"></a></label></li>
                                                <li><label><input type="checkbox"><i></i><span>New Order </span><a href='#' class="ti-close"></a></label></li>
                                                <li><label><input type="checkbox" checked><i></i><span>Home Delevery</span><a href='#' class="ti-close"></a></label></li>
                                                <li><label><input type="checkbox" checked><i></i><span>New Chicken Receipe</span><a href='#' class="ti-close"></a></label></li>
                                            </ul>
                                        </div>
                                        <input type="text" class="tdl-new form-control" placeholder="Write new item and hit 'Enter'...">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /# column -->
                        
                    </div><!-- /# row -->
				</div><!-- /# main content -->
            </div><!-- /# container-fluid -->
        </div><!-- /# main -->
    </div><!-- /# content wrap -->
    <!-- chart js input -->
    

    <script type="text/javascript">
        new Chart(document.getElementById("bar-chart-horizontal"), {
            type: 'horizontalBar',
            data: {
              labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
              datasets: [
                {
                  label: "Population (millions)",
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                  data: [2478,5267,734,784,433]
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Predicted world population (millions) in 2050'
              }
            }
        });
        new Chart(document.getElementById("line-chart"), {
            type: 'line',
            data: {
                labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
                datasets: [{ 
                data: [86,114,106,106,107,111,133,221,783,2478],
                label: "Africa",
                borderColor: "#3e95cd",
                fill: false
              }, { 
                data: [282,350,411,502,635,809,947,1402,3700,5267],
                label: "Asia",
                borderColor: "#8e5ea2",
                fill: false
              }, { 
                data: [168,170,178,190,203,276,408,547,675,734],
                label: "Europe",
                borderColor: "#3cba9f",
                fill: false
              }, { 
                data: [40,20,10,16,24,38,74,167,508,784],
                label: "Latin America",
                borderColor: "#e8c3b9",
                fill: false
              }, { 
                data: [6,3,2,2,7,26,82,172,312,433],
                label: "North America",
                borderColor: "#c45850",
                fill: false
              }
            ]
          },
          options: {
            title: {
              display: true,
              text: 'World population per region (in millions)'
            }
          }
        });
        /*new Chart(document.getElementById("doughnut-chart"), {
            type: 'doughnut',
            data: {
              labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
              datasets: [
                {
                  label: "Population (millions)",
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                  data: [2478,5267,734,784,433]
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Predicted world population (millions) in 2050'
              }
            }
        });*/
        var canvasP = document.getElementById("pieChart");
        var ctxP = canvasP.getContext('2d');
        var myPieChart = new Chart(ctxP, {
           type: 'pie',
           data: {
              labels: ["Värde 1", "Värde 2", "Värde 3", "Värde 4", "Värde 5", "Värde 6", "Värde 7"],
              datasets: [{
                 data: [1, 5, 10, 20, 50, 70, 50],
                 backgroundColor: ["#64B5F6", "#FFD54F", "#2196F3", "#FFC107", "#1976D2", "#FFA000", "#0D47A1"],
                 hoverBackgroundColor: ["#B2EBF2", "#FFCCBC", "#4DD0E1", "#FF8A65", "#00BCD4", "#FF5722", "#0097A7"]
              }]
           },
           options: {
              legend: {
                 display: true,
                 position: "right"
              }
           }
        });

        canvasP.onclick = function(e) {
           var slice = myPieChart.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Värde 5':
                 alert('clicked on slice 5');
                 //window.open('www.example.com/foo');
                 break;
              case 'Värde 6':
                 alert('clicked on slice 6');
                 //window.open('www.example.com/bar');
                 break;
              // add rests ...
           }
        }
    </script>



<?php include('footer.html'); ?>






<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>ExpenseDocs</title>
	
	<!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="./UI-SamplePages/http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="./UI-SamplePages/http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="./UI-SamplePages/http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon--> 
    <link rel="apple-touch-icon" sizes="72x72" href="./UI-SamplePages/http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon--> 
    <link rel="apple-touch-icon" sizes="57x57" href="./UI-SamplePages/http://placehold.it/57.png/000/fff">
	
	<!-- Styles -->
    <link href="./UI-Tools/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/themify-icons.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/weather-icons.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/mmc-chat.css" rel="stylesheet" />
    <link href="./UI-Tools/css/lib/sidebar.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="./UI-Tools/css/lib/simdahs.css" rel="stylesheet">
    <link href="./UI-Tools/css/style.css" rel="stylesheet">


    
    <!-- Scripts -->
    <script src="./UI-Tools/js/lib/jquery.min.js"></script><!-- jquery vendor -->
    <script src="./UI-Tools/js/lib/jquery.nanoscroller.min.js"></script><!-- nano scroller -->    
    <script src="./UI-Tools/js/lib/sidebar.js"></script><!-- sidebar -->
    <script src="./UI-Tools/js/lib/bootstrap.min.js"></script><!-- bootstrap -->
    <script src="./UI-Tools/js/lib/mmc-common.js"></script>
    <script src="./UI-Tools/js/lib/mmc-chat.js"></script>
    <!--  Chart js -->
    <!-- <script src="./UI-Tools/js/lib/chart-js/Chart.bundle.js"></script>
    <script src="./UI-Tools/js/lib/chart-js/chartjs-init.js"></script> -->
    
    <!-- // Chart js -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css" >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>



    <script src="./UI-Tools/js/lib/sparklinechart/jquery.sparkline.min.js"></script><!-- scripit init-->
    <script src="./UI-Tools/js/lib/sparklinechart/sparkline.init.js"></script><!-- scripit init-->
    
    <!--  Datamap -->
    <script src="./UI-Tools/js/lib/datamap/d3.min.js"></script>
    <script src="./UI-Tools/js/lib/datamap/topojson.js"></script>
    <script src="./UI-Tools/js/lib/datamap/datamaps.world.min.js"></script>
    <script src="./UI-Tools/js/lib/datamap/datamap-init.js"></script>
    <!-- // Datamap -->
    <script src="./UI-Tools/js/lib/weather/jquery.simpleWeather.min.js"></script>   
    <script src="./UI-Tools/js/lib/weather/weather-init.js"></script>
    <script src="./UI-Tools/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="./UI-Tools/js/lib/owl-carousel/owl.carousel-init.js"></script>
    <!-- <script src="./UI-Tools/js/scripts.js"></script> --><!-- scripit init-->

    
</head>

<body>
    
    <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
        <div class="nano">
            <div class="nano-content">
                <ul>
                    <li class="label" id="pageTitle">Main</li>
                    <li class="active"><a href="./index.html"><i class="ti-home"></i> Dashboard </a></li>   
                    
                    <li><a href="./onboarding.html"><i class="ti-file"></i> Onboarding</a></li>
                    
                    <li><a href="./UI-SamplePages/app-profile.html"><i class="ti-user"></i> Profile</a></li>
                    
                    <li><a class="sidebar-sub-toggle"><i class="ti-layout"></i> Sales <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                        <ul>
                            <li><a href="./invoice-page.html">Invoice page</a></li>
                            
                        </ul>
                    </li>
                    <li><a class="sidebar-sub-toggle"><i class="ti-panel"></i> Finance <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                        <ul>
                            <li><a href="./dashboard-page.html">Dashboard Page</a></li>
                            <li><a href="./bank-master.html">Bank Master</a></li>
                            <li><a href="./bank-transaction.html">Transaction</a></li>
                            <li><a href="./bank-statement-mapping.html">Statement Mapping</a></li>
                            <li><a href="./bank-payment-confirmation.html">Payment Confirmation</a></li>
                        </ul>
                    </li>
                        
                    
                    <li><a class="sidebar-sub-toggle"><i class="ti-layout-grid4-alt"></i> Document <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                        <ul>
                            <li><a href="./upload-document.html">Upload document</a></li>
                            <li><a href="./uploaded-document.html">Uploaded document</a></li>
                            <li><a href="./upload-document-multiline-breakdown.html">Upload document multiline breakdown</a></li>
                            
                        </ul>
                    </li>
                    

                    <!-- <li><a class="sidebar-sub-toggle"><i class="ti-target"></i> Pages <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                        <ul>
                            <li><a href="./page-login.html">Login</a></li>
                            <li><a href="./page-register.html">Register</a></li>
                            <li><a href="./page-reset-password.html">Forgot password</a></li>
                        </ul>
                    </li> -->
                    
                    <li><a href="./scanned-document-dashboard.html"><i class="ti-file"></i> Scanned document dashboard</a></li>

                    <li><a href="./posting-data-accounting-software.html"><i class="ti-file"></i> Posting data accounting software</a></li>

                    <li><a><i class="ti-close"></i> Logout</a></li>

                </ul>
            </div>
        </div>
    </div><!-- /# sidebar -->
    

    <div class="header">
        <div class="pull-left">
            <div class="logo" id="sideLogo">
                <a href="#">
                    <img class="full-logo" src="./UI-Tools/images/logo-big-new-w.png" alt="SimDahs">
                    <img class="small-logo" src="./UI-Tools/images/logo-small-new-w.png" alt="SimDahs">
                </a>
            </div>
            <div class="hamburger sidebar-toggle">
                <span class="ti-menu"></span>
            </div>
        </div>

        <div class="pull-right p-r-15">
            <ul>
                <li class="header-icon dib"><i class="ti-bell"></i>
                    <div class="drop-down">
                        <div class="dropdown-content-heading">
                            <span class="text-left">Recent Notifications</span>
                        </div>
                        <div class="dropdown-content-body">
                            <ul>
                                <li>
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/3.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">Mr. Raizer</div>
                                            <div class="notification-text">5 members joined today </div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/3.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">Sofiya</div>
                                            <div class="notification-text">likes a photo of you</div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/3.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">Richard</div>
                                            <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/3.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">Sara Maggi</div>
                                            <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="text-center">
                                    <a href="./UI-SamplePages/#" class="more-link">See All</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="header-icon dib"><i class="ti-email"></i>
                    <div class="drop-down">
                        <div class="dropdown-content-heading">
                            <span class="text-left">2 New Messages</span>
                            <a href="./UI-SamplePages/email.html"><i class="ti-pencil-alt pull-right"></i></a>
                        </div>
                        <div class="dropdown-content-body">
                            <ul>
                                <li class="notification-unread">
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/1.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">John Doe</div>
                                            <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                        </div>
                                    </a>
                                </li>

                                <li class="notification-unread">
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/2.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">Sara Maggi</div>
                                            <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/3.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">John Doe</div>
                                            <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="./UI-SamplePages/#">
                                        <img class="pull-left m-r-10 avatar-img" src="./UI-Tools/images/avatar/2.jpg" alt="" />
                                        <div class="notification-content">
                                            <small class="notification-timestamp pull-right">02:34 PM</small>
                                            <div class="notification-heading">Sara Maggi</div>
                                            <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="text-center">
                                    <a href="./UI-SamplePages/#" class="more-link">See All</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
				<li class="header-icon dib chat-sidebar-icon"><i class="ti-comment"></i></li>
                <li class="header-icon dib"><img class="avatar-img" src="./UI-Tools/images/avatar/1.jpg" alt="" /> <span class="user-avatar">Wolf <i class="ti-angle-down f-s-10"></i></span>
                    <div class="drop-down dropdown-profile">
                        <div class="dropdown-content-heading">
                            <span class="text-left">Upgrade Now</span>
                            <p class="trial-day">30 Days Trail</p>
                        </div>
                        <div class="dropdown-content-body">
                            <ul>
                                <li><a href="./UI-SamplePages/#"><i class="ti-user"></i> <span>Profile</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-wallet"></i> <span>My Balance</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-write"></i> <span>My Task</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-calendar"></i> <span>My Calender</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-email"></i> <span>Inbox</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-settings"></i> <span>Setting</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-help-alt"></i> <span>Help</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-lock"></i> <span>Lock Screen</span></a></li>
                                <li><a href="./UI-SamplePages/#"><i class="ti-power-off"></i> <span>Logout</span></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

	<div class="chat-sidebar">
        <!-- BEGIN chat -->
        <div id="mmc-chat" class="color-default">
            <!-- BEGIN CHAT BOX -->
            <div class="chat-box">
                <!-- BEGIN CHAT BOXS -->
                <ul class="boxs"></ul>
                <!-- END CHAT BOXS -->
                <div class="icons-set">
                    <div class="stickers">
                        <div class="had-container">
                            <div class="row">
                                <div class="s12">
                                    <ul class="tabs" style="width: 100%;height: 60px;">
                                        <li class="tab col s3">
                                            <a href="#tab1" class="active">
                                                <img src="./images/1.jpg" alt="" />
                                            </a>
                                        </li>
                                        <li class="tab col s3"><a href="#tab2">Test 2</a></li>
                                    </ul>
                                </div>
                                <div id="tab1" class="s12 tab-content">
                                    <ul>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                        <li><img src="images/1.png" alt="" /></li>
                                    </ul>
                                </div>
                                <div id="tab2" class="s12 tab-content">Test 2</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CHAT BOX -->
            <!-- BEGIN SIDEBAR -->
            <div id="sidebar" class="right scroll">
                <div class="had-container">
                    <!-- BEGIN USERS -->
                    <div class="users">

                        <ul class="user-list">
                            <!-- BEGIN USER-->
                            <li class="user-tooltip" data-id="1" data-status="online" data-username="Rufat Askerov" data-position="left" data-filter-item data-filter-name="rufat askerov">
                                <!-- BEGIN USER IMAGE-->
                                <div class="user-image">
                                    <img src="./UI-Tools/images/avatar/1.jpg" class="avatar" alt="Rufat Askerov" />
                                </div>
                                <!-- END USER IMAGE-->
                                <!-- BEGIN USERNAME-->
                                <span class="user-name">Rufat Askerov</span>
								<span class="user-show"></span>
                                <!-- END USERNAME-->
                            </li>
                            <!-- END USER-->
                            <!-- BEGIN USER-->
                            <li class="user-tooltip" data-id="3" data-status="online" data-username="Alice" data-position="left" data-filter-item data-filter-name="alice">
                                <div class="user-image">
                                    <img src="./UI-Tools/images/avatar/1.jpg" class="avatar" alt="Alice" />
                                </div>
                                <span class="user-name">Alice</span>
								<span class="user-show"></span>
                            </li>

                            <!-- BEGIN USER-->
                            <li class="user-tooltip" data-id="7" data-status="offline" data-username="Michael Scofield" data-position="left" data-filter-item data-filter-name="michael scofield">
                                <div class="user-image">
                                    <img src="./UI-Tools/images/avatar/1.jpg" class="avatar" alt="Michael Scofield" />
                                </div>
                                <span class="user-name">Michael Scofield</span>
								<span class="user-show"></span>
                            </li>

                            <!-- BEGIN USER-->
                            <li class="user-tooltip" data-id="5" data-status="online" data-username="Irina Shayk" data-position="left" data-filter-item data-filter-name="irina shayk">
                                <div class="user-image">
                                    <img src="./UI-Tools/images/avatar/1.jpg" class="avatar" alt="Irina Shayk" />
                                </div>
                                <span class="user-name">Irina Shayk</span>
								<span class="user-show"></span>
                            </li>

                            <!-- BEGIN USER-->
                            <li class="user-tooltip" data-id="6" data-status="offline" data-username="Sara Tancredi" data-position="left" data-filter-item data-filter-name="sara tancredi">
                                <div class="user-image">
                                    <img src="./UI-Tools/images/avatar/1.jpg" class="avatar" alt="Sara Tancredi" />
                                </div>
                                <span class="user-name">Sara Tancredi</span>
								<span class="user-show"></span>
                            </li>

                            <!-- BEGIN USER-->
                            <li class="user-tooltip" data-id="7" data-status="offline" data-username="Wolf" data-position="left" data-filter-item data-filter-name="Wolf">
                                <div class="user-image">
                                    <img src="./UI-Tools/images/avatar/1.jpg" class="avatar" alt="Wolf" />
                                </div>
                                <span class="user-name">Wolf</span>
								<span class="user-show"></span>
                            </li>
                        </ul>
						<div class="chat-user-search">
							<div class="input-group">
								<span class="input-group-addon"><i class="ti-search"></i></span>
								<input type="text" class="form-control" placeholder="Search"  data-search />
							</div>
						</div>
                    </div>
                    <!-- END USERS -->

                </div>
            </div>
            <!-- END SIDEBAR -->
        </div>
        <!-- END chat -->
    </div>
    <!-- END chat Sidebar-->